import React,{ Component } from 'react';
import {
    View, Text, TouchableOpacity, Image, TextInput, StyleSheet, Dimensions
} from 'react-native';

import back from '../img/back.png';
import icsearch from '../img/Search.png';
import global from '../global';

const {height} = Dimensions.get('window');


export default class Search extends Component {
    constructor(props) {
        super(props);
       this.state = {
           text: ''
       };
    }
    _search = () => {
        //const {state} = this.props.navigation;
        global.keysearch = this.state.text;
        this.props.navigation.state.params.constsearch();
        this.props.navigation.goBack();
    }
    render() {
    const {goBack} = this.props.navigation;
    const {setParams} = this.props.navigation;
        return (
            <View style={style.header}>
              <View style={{flex: 1}}>
                <TouchableOpacity
                  onPress={() => goBack()}
                >
                  <Image
                    style={style.imgHeader}
                    source={back}
                  />
                </TouchableOpacity>
              </View>

              <View style={{flex: 7}}>
                <TextInput
                  placeholder="Tìm kiếm"
                  underlineColorAndroid="transparent"
                  value={this.state.keysearch}
                  onChangeText={(text) => this.setState({text})}
                >

                </TextInput>
              </View>

              <View style={{flex: 1}}>
                <TouchableOpacity
                  onPress={this._search}
                >
                  <Image
                    style={style.imgHeader}
                    source={icsearch}
                  />
                </TouchableOpacity>
              </View>
            </View>
        )
    }
    componentDidMount(){

    }
}
var style = StyleSheet.create({
  header:{
      flexDirection: 'row',
      backgroundColor: 'white',
      height: height / 10,
      padding: 10,
      alignItems: 'center',
      justifyContent: 'space-between'
  },
  imgHeader:{
    height: 25,
    width: 25
  }
})
