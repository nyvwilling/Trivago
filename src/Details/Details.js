import React, { Component } from 'react';
import {
    View, Text, TouchableOpacity
} from 'react-native';

export default class Details extends Component {
    render() {
      const {goBack} = this.props.navigation;
        return (
            <View>
                <Text>Details Screen</Text>
                <TouchableOpacity
                  onPress={() => goBack()}
                ><Text>Go back</Text></TouchableOpacity>
            </View>
        )
    }
}
