import React from 'react';
import {
    Dimensions
} from 'react-native';
import { StackNavigator, DrawerNavigator } from 'react-navigation';
import Main from './Main/Main';
import Menu from './Menu/Menu';
import Account from './Account/Account';
import Search from './Search/Search';
import Details from './Details/Details';
import Welcome from './Welcome/Welcome';
import MapView from './MapView/Map';

var { height, width } = Dimensions.get('window');

export const MainStack = StackNavigator({
    MainScreen:{
        screen: Main,
        navigationOptions: {
            header: null
        }
    },
    AccountScreen:{
        screen: Account,
        navigationOptions: {
            headerTitle: 'Account'
        }
    },
    SearchScreen:{
        screen: Search,
        navigationOptions: {
            header: null
        }
    },
    DetailScreen:{
        screen: Details
    },
    MapScreen:{
        screen: MapView
    }

});

export const SideMenu = DrawerNavigator ({
        MainSide: {
            screen: MainStack
        }
    },
    {
        drawerWidth: 0.8 * width,
        drawerPosition: 'left',
        contentComponent: props => <Menu {...props} />
    }
)
export const WelcomeStack = StackNavigator({
    WelcomeScreen:{
        screen: Welcome,
        navigationOptions: {
            header: null
        }
    },
    MainScreen:{
        screen: SideMenu,
        navigationOptions: {
            header: null
        }
    },
    AccountScreen:{
      screen: Account,
      navigationOptions:{
        headerTitle: 'Account'
      }
    }

});
/*const prevGetStateForAction = WelcomeStack.router.getStateForAction;
WelcomeStack.router.getStateForAction = (action, state) => {
  // Do not allow to go back from Welcome
  if (action.type === "Navigation/BACK" && state && state.routes[state.index].routeName === "WelcomeScreen") {
    return null;
  }
  // Do not allow to go back to Welcome
  if (action.type === "Navigation/BACK" && state) {
    const newRoutes = state.routes.filter(r => r.routeName !== "WelcomeScreen");
    const newIndex = newRoutes.length - 1;
    return prevGetStateForAction(action, { index: newIndex, routes: newRoutes });
  }
  return prevGetStateForAction(action, state);
};

const prevGetStateForAction1 = MainStack.router.getStateForAction;
WelcomeStack.router.getStateForAction = (action, state) => {
  // Do not allow to go back from Search
  if (action.type === "Navigation/BACK" && state && state.routes[state.index].routeName === "SearchScreen") {
    return null;
  }
  // Do not allow to go back to Search
  if (action.type === "Navigation/BACK" && state) {
    const newRoutes = state.routes.filter(r => r.routeName !== "SearchScreen");
    const newIndex = newRoutes.length - 1;
    return prevGetStateForAction(action, { index: newIndex, routes: newRoutes });
  }
  return prevGetStateForAction(action, state);
};*/
