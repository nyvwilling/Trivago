import React, {Component} from 'react';
import {
  View, Text, Image, Dimensions,
  StyleSheet, TextInput, TouchableOpacity
 } from 'react-native';

import user from '../img/user.png';
import nameapp from '../img/name-app.png';
import logen from '../img/logen.png';
import search from '../img/iconsearch.png';
import global from '../global';

const { height } = Dimensions.get('window');

export default class Welcome extends Component{
  constructor(props){
    super(props);
    this.state = {
      text: ''
    };
  }
  WelSearch = () =>{
    global.keysearch = this.state.text;
    this.props.navigation.navigate('MainScreen');
  }
  render(){
    const { navigate } = this.props.navigation;
    return(
      <View style={{backgroundColor:"#FFFFFF"}}>
        <View style={{height:height/3}}>
          <View style={{alignItems:"flex-end"}}>
            <TouchableOpacity
              onPress={()=> { navigate('AccountScreen', { param: 'Hoang Van Cong' })}}
            >
              <Image style={style.img}
                source={user}
              />
            </TouchableOpacity>
          </View>
          <View style={{alignItems:"center"}}>
            <Image
              source={nameapp}
            />
          </View>
        </View>

        <View style={{alignItems:"center", height:height/3}}>
          <View>
            <Image
              source={logen}
            />
          </View>
          <View style={style.khungsearch}>
            <View style={{flex:1}}></View>
            <View style={{flex:6, margin:3}}>
              <TextInput style={style.search}
                placeholder="Bạn muốn đi đâu?"
                underlineColorAndroid="transparent"
                value={this.state.key}
                onChangeText={(text) => this.setState({text})}
              >
              </TextInput>
            </View>
            <View style={{flex:1, margin:10}}>
              <TouchableOpacity
                onPress={this.WelSearch}
              >
                <Image style={style.iconsearch}
                  source={search}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>

        <View style={{height:height/3}}>
        </View>
      </View>
    )
  }
}
var style = StyleSheet.create({
  img: {
    width: 30,
    height: 30,
    margin: 5
  },
  row:{
    flex:1
  },
  khungsearch:{
    flexDirection:"row"
  },
  search:{
    borderWidth: 3,
      borderColor: '#fafafa'
  },
  iconsearch:{
      width: 30,
      height: 30
  }
});
