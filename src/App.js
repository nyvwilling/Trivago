import React, { Component } from 'react';
import {
    View,
    Text
} from 'react-native';

import { WelcomeStack } from './Router';
export default class App extends Component {
    render() {
        return (
            <WelcomeStack />
        )
    }
}
