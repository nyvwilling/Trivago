import MapView from 'react-native-maps';
import React, {Component} from 'react';
import {Text, View, StyleSheet, Dimensions} from 'react-native';
const { height, width } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: height/3,
    width: width,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});

export default class Map extends Component{
  render() {
    const { region } = this.props;
    console.log(region);

    return (
      <View style ={styles.container}>
        <MapView
          style={styles.map}
          region={{
            latitude: 10.8469059,
            longitude: 106.7979073,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121,
          }}
        >
        <MapView.Marker coordinate={{
            latitude: 10.8469059,
            longitude: 106.7979073
        }}/>
        </MapView>
      </View>
    );
  }
}
